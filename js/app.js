require.config({
    "baseUrl": "js/lib",
    "paths": {
        "app": "../app",
        // You can set a local fall back in case CDN doesn't load
        "jquery": [
            "//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min",
            "jquery-1.10.2.min"
        ],
        "jquery.ui": [
            "//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min",
            "jquery-ui-1.11.2.min"
        ],
        "modernizr": "modernizr-2.6.2.min",
    },
    // Set load dependencies using shim
    shim: {
        "jquery.ui": ["jquery"]
    }
});

// Load the main app module to start the app
require(["app/main"]);
